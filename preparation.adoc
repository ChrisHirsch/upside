= Preparing and testing your development environment =

Terminology: your "host machine" is your main Linux desktop or laptop. Your
"test machine" is the SBC where upsided will run.  This document
assumes you are at your host machine.

== Host machine setup ==

One of your options will be to cross-develop, building on your host
and copying the binaries to the test machine.  You may also want to
set up a native build environment on the test machine.

1. Install Go: "apt-get install golang" under Debian derivatives.

2. cd into the upsided/ directory and 'make setup'. This will install Gobot.

== Test machine setup on a BeagleBone Black ==

=== Powering the BeagleBone up and down ===

First, connect the test machine your local network with an Ethernet cable.

Out of the box the BB is powered through its gadget port by a cable
with a USB A plug on one and and a mini-B plug on the other; these
are normally shipped with the device. Your power the machine up simply
by plugging it the mini-B into the board and the A into a USB port on
your host machine. A blue power LED will go on steady when you do this.

There is a power button, helpfully marked, next to the Ethernet
connector. When powering off the board you should either "shutdown -h
now" or hold down the power button until the power LED near the 5V
connector goes off. If you just unplug the unit there is a small but
nonzero chance that the software in eMMC (the onboard nonvolatile
memort) will be corrupted.

If you're going to work with the gadget port, you'll need to power it
through the female barrel plug next to the Etherenet port - the one
marked "5V" in large friendly letters. This wants a 5VDC 1.2AMP power
supply of the kind shipped with every home router in the world -
coaxial with 2.1mm ID and 5.5mm OD.

=== Operating system setup ===

1. Smoke-test your BeagleBone.  Power it up, with no SD card inserted. After
a few moments you should see blinking, fluttering activity on the four
blue LEDs to the right of the Ethernet port.

2. Verify that, on the SBC's Ethernet connector, both link and
activity lights are on.  If they're not, check your network
connection.

3. Point the browser on your host at http:://beaglebone.local to
verify that the machine is live without involving sshd or password
checking.  You may have to wait a minute or so for dynamic DNS to get
its act together and sshd to start running.

4. ssh from your host machine.  Probably the magic combination will be
usr 'debian@beaglebone.local' and password 'temppwd'.  Some older
images expected you to log in as root@beaglebone.local, with no
password set; you won't see this unless your BB has been sitting on a
shelf for a while.

5. Note: If you get a "hostame not found" or "port 22: Connection
refused" message while trying to ssh in, this means, respectively that
mDNS and the Beaglebone are trying to get their act together about the
"beaglebone.local" address or sshd isn't quite up yet. Be patient, it
will unjam.

6. cat /etc/dogtag.  Make a note of the system release information.

7. Now that you know the SBC is sane, reflash it with a current
image.  From https://beagleboard.org/latest-images get the latest IoT
image for  BeagleBone; IoT means without graphical desktop, which
you won't need. Use sha256sum to check it's correct.

8. Plug in a USB SD card writer, with an SD in it, on your host
machine.  Get the ddimage script via this command on your host:

wget https://www.ntpsec.org/white-papers/stratum-1-microserver-howto/ddimage

and use it to put the image on your SD card.

9. "shutdown -h now" on the BB, unplug the power on the BB and insert
the SD card in its holder.  Power on. When you can ssh in, look at
/media/rootfs/etc/dogtag which is the release information for the new
image. It should differ from what you saw in step 6.  This is how you
check that the new image has a valid filesysem.

10. Go root on the test machine.  Edit /media/rootfs/boot/uEnv.txt
uncommenting the line that reads

cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh'

This will alter the SD image into one that reflashes the eMMC on the BB.
Do "shutdown -h now" and wait for the LEDs to go dark, to be sure the
flash write gets done. Disconnect power to the BB.

11. Hold down the boot button, just above and a bit to the right of
the SD. Plug in the power; you can release the button when the power
LED comes on.  The reflash process seems to be indicated by the LEDs
doing a ripple effect back and forth that is unlike the normal
flicker. It can take 15 minutes or more. When the reflash is finished,
all LEDs will go solid for a bit, then dark.

12. Do ssh in and "shutdown -h now". Remove the SD. Power up and ssh
in.  Now /etc/dogtag should indicate the new system, matching what you
saw in step 9.

13. "apt-get update and apt-get upgrade" to bring your software up to
date.

14. Change the password for the default 'debian' user.

15. Disable the unneeded wifi support (the BB Black has no wifi
hardware).  Run

systemctl disable wpa_supplicant.service --global

then remove the file

/usr/share/dbus-1/system-services/fi.w1.wpa_supplicant1.service

If you don't remove this file, the service may be restarted by a dbus
request; see

https://unix.stackexchange.com/questions/306276/make-systemd-stop-starting-unwanted-wpa-supplicant-service

for details.

16. Create a user account for yourself on the test machine using the
   same name as you do on your host machine (if it's different, the
   cross-compile-and-execute script we'll introduce in a later ).  Add your
   user id to the sudo group in /etc/group.  Also add it to the gpio
   and pwm groups to get full access to the hardware.

== Test machine native build setup ==

Optionally, these steps on your test machine to have a full
development environment there.

1. Install Go: "apt-get install golang".

3. Copy your .gitconfig and .ssh directory to $HOME with scp.  You'll need
   these to authenticate yourself to GitLab.

4. Log out of root and debian and ssh to your user account.

5. Clone the UPSide repository: git clone git@gitlab.com:esr/upside.git

6. cd to $HOME/upside and 'make setup'. This will install
   prerequisites for building the daemon.

== Initial test ==

(This adapts the test described at
https://github.com/hybridgroup/gobot/tree/master/platforms/beaglebone)

In the upsided directory on your host machine, run "bin/remote/blinktest".
You should see progress messages from the blinktest robot running on
the test machine.  And you should see the usr1 LED blink.  If you
don't, make sure you're in the gpio group - without that you can't 
work the GPIO pins.

// Local Variables:
// mode: doc
// End:

