= Transactional design of UPSide =

== Forebrain/midbrain/hindbrain partitioning ==

For reasons including but not limited to by the cost of getting
regulatory approvals, the electronics is partitioned into low-power
and high-power electronics with very narrow interfaces among them.

Any change to the high-power subsystems could require a new approval
cycle. Therefore, as much as possible of the system logic - and all
policy logic - lives in the low-power part.  The high-power subsystem
is as dumb and simple as possible; keeping it dumb and simple is
a principal design goal.

The "midbrain" is the line-voltage unit: AC in, AC out, DC in, DC out,
and a data bus to the forebrain. As little policy as possible is done
here, since any change probably leads to a recertification cycle. It
is where the "safety" rules are enforced, like only one source feeding
the equipment. The midbrain needs to fail to a safe state if both
battery and mains current die.

The "forebrain" is responsible for external interfacing, including
driving the status display and the USB port.

The "hindbrain" is the battery management system.  This term was
coined at a time when we were not yet aware of BMS chips and the Smart
Battery Standard; now, the hindbrain will consist of one of these and
its associated sensors inboard of the battery.

The UPSide design uses an embedded Unix computer as a forebrain, which
puts some constraints on it. It can't have a filesystem that is both
writeable and persistent across boots - otherwise the UPS will
eventually fall in a cryptic Unix way after a power outage.  We solve
this problem with a boot sequence based on initramfs.

Everything else is driven by the forebrain, either directly or through
bus messages to/from the midbrain.

== Battery management ==

An important variable of any UPS design is where the battery
management is done.  There are are three cases:

1. LiFePo batteries normally ship with a battery controller on a
little PCB inboard of the battery. Its purpose in life is to enforce a
cutoff voltage so the battery doesn't get deep-cycled, and rate-limit
charging of the battery so it doesn't get damaged.  The interface to
the rest of the system is just 4 DC connections; the inboard
controller doesn't ship any data out.

2. Some batteries, especially lithium-ion batteries, ship with a much
smarter inboard battery controller that has a digital channel supplying
detailed statistics and battery modeling.  There is a "Smart Battery
Standard" that describes a two-wire digital interface to a smart
battery standard.

3. Lead-acid batteries, including the SLAs and gel-cel variants used
in UPSes, usually have no inboard BMS at all - the DC input and output
go straight to the plates. The UPS has to supply its own BMS, but at
least in this case it's possible to standardize one.

We refer to the battery controller as the "hindbrain".  The UPSide-1
design assumes case 2, an SBS-compatible hindbrain.

Batteries for case 3 would be cheaper, but we want to avoid the
liability and complexity issues of building our own BMS.

== Subsystems ==

Forebrain::
An SBC running Unix, the master controller.

Midbrain::
Control logic for the high-power subsystem.

Hindbrain::
The battery management system.

Display::
Human-readable status display.  A 20x4 LCD panel or something in that class.

Buzzer::
Alarm-tone generator, controlled by forebrain.

Bus::
Data bus connecting the subsystems - I2C/TWI, SPI, SMBUS, or something
equivalent.

The following diagram shows data flow among the low-power components.
Power connections are not indicated.  Arrows carry
message traffic.

image:paths.svg[]

The above is slightly simplifed; in implementation, the "Bus" may be
seeral two-wire buses (I2C/TWI or SMBUS).  Also, logic paths for the
UI control buttons are not shown.

== Master state diagram ==

This section describes the behavior and state transitions of the
UPSide.  It is deliberately not an ECAD design and includes no part
numbers; the intent is to specify system partitioning and behavior,
not implementation.

These policy parameters control the state transitions:

[options="header"]
|========================================================================
| Parameter          | Default  | Meaning
| ShutdownTime       | 30 sec	| Time required for host shutdown
| WarningTime        | 60 sec	| Desired warning margin before shutdown
| HostDrainThreshold | 10V      | Lowest expected draw of shut down host.
|========================================================================

In the following state diagram, ovals are are possible values of a
single "readiness" state and boxes are state transition events.

"Bus:" lines in events are identifiers for event notifications.
Semantics of event notifications is described in a later section.

"Display:" lines in events are text to be sent to the status display.

"Alarm:" lines in events are alarm codes.  Those are described in a
later section.

Not shown is that in normal operation the forebrain periodically sends
ALIVE to the midbrain to prevent the watchdog from rebooting the daemon.

image:flow.png[]

Note: Due to measurement limits in SBS 1.1, we must assume that
time-to-empty reports may be inaccurate by up to two minutes
and allow that margin.

== Bus messages and alarm codes ==

[options="header"]
|========================================================
|Code     | From      | To        | Payload
|BUZZ     | Forebrain | Midbrain  | Buzzer tone sequence (freq/duration pairs)
|ALIVE    | Forebrain | Midbrain  | Prevent watchdog reboot
|ENABLE   | Forebrain | Midbrain  | Enable AC output
|DISPLAY  | Forebrain | Display   | Text for display
|========================================================

For possible messages between forebrain and hindbrain, see the
http://sbs-forum.org/specs/sbdat110.pdf[Smart Battery Standard].

=== BUZZ ===

BUZZ is what the forebrain uses to ring alarms; all alarms except FAIL
are done this way.  It is used to implement the following:

|========================================================
|CHARGING | Waiting on battery charge   | R in Morse code
|UP       | Mains power on or restored  | U in Morse code
|DOWN     | Going to battery power      | D in Morse code
|RESTORED | Power restored              | K in Morse code
|SOS      | Dwell limit approaching     | SOS prosign in Morse code
|SHUTDOWN | Dwell limit imminent        | BREAK prosign in Morse code
|========================================================

== Hardware block design ==

Selected subsystems are described following the diagram. Diamonds
label control or sensor termini in the forebrain,

image:block.svg[]

"mains" monitors mains power (read only, polled).

"bms" monitors the BMS (read/write. polled).

"sens0" monitors the 12VDC load (read only, polled).  CTRL0
is is

"sens[1...n]" channels monitor the AC load on each output (read only, polled).

"sound" is the alarm-tone generator (write only).

"display" is the message display (write only).

"alive" receives keepalive messages from the forebrain. If the timer loop
(8) overruns, the forebrain power supply (7) is told to cycle power
to the SBC (write only).

"ctrl" channels control power output and are used to implement
load-shedding logic in the forebrain software (write only).

== Policy daemon architecture ==

The upside policy daemon, 'upsided', is the custom software component
of the UPSide system.  It works within a non-customized Linux OS resident
on the controller SBC. The only specialized requirement on the OS
is that GPIO, I2C and SMBUS drivers have to be present and working.

The daemon is reponsible for (a) mediating between the high-power
hardware on the UPSide midbrain and interfaces to the host (including in
particular the USB control and reporting channel), and (b)
implementing the transitions described in the master state diagram.

The daemon's interface to its environment consists of:

* A collection of Unix devices representing I2C/SMBUS
  two-wire buses linking the forebrain to the rest of the system via
  the channels starting or ending with diamonds in the block diagram.

* The GPIO driver, which accesses the state of pins wired to UI control
  buttons.

* The device corresponding to the gadget port for the USB uplink to
  the host.

The main loop of the daemon repeatedly executes the following steps:

1. Initialize a a global structure, Observed, describing powerplane
   state. Full the members with a magic value meaning "unknown".

2. Read all sensors.  For each one,

   a. Cook the data as needed and post it to 
   describing the 

   b. Compute whether comparing Observed with LastObserved fires any
   events (critical level changes in voltages, thresholds in the
   battery charge level.) Feed the implied events to the policy logic.

3. Pause to avoid buzzing the bus.  Copy Observed to LastObserved.

// Local Variables:
// mode: doc
// End:
