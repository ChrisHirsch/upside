// This file is Copyright (c) 2018 by the UPSide project.
// SPDX-License-Identifier: BSD-2-clause
//
// Smart Battery Data 1.1 data through the Linux sys-battery driver
// via nodes in the Linux device tree under sys.

package sbd

import (
	"fmt"
	"io/ioutil"
)

func nodeName(bat int) string {
	return fmt.Sprintf("/sys/class/power_supply/BAT%d/", bat)
}

func queryNode(bat int, fld string) []byte {
	b, e := ioutil.ReadFile(nodeName(bat) + fld)
	if e != nil {
		panic(e)
	}
	return b
}

/*
 * FromSysTree reads data for a specified field out of the sys tree
 */
func (ctx *SBDContext) FromSysTree(bat int, msgtype string) *SBDMessage {
	query := func (mt string) []byte {
		return queryNode(bat, mt)
	}
	squery := func (mt string) string {
		return string(queryNode(bat, mt))
	}
	msg := new(SBDMessage)
	switch msgtype {
	case "RemainingCapacityAlarm":
	case "RemainingTimeAlarm":
	case "BatteryMode":
	case "AtRate":
	case "AtRateTimeToFull":
	case "AtRateTimeToEmpty":
	case "AtRateOK":
	case "Temperature":
	case "Voltage":
	case "Current":
	case "AverageCurrent":
	case "MaxError":
	case "RelativeStateOfCharge":
	case "AbsoluteStateOfCharge":
	case "RemainingCapacity":
	case "FullChargeCapacity":
	case "RunTimeToEmpty":
	case "AverageTimeToEmpty":
	case "AverageTimeToFull":
	case "BatteryStatus":
	case "CycleCount":
		fmt.Sscanf(squery("cycle_count"), "%d", &msg.UnsignedIntArg)
	case "DesignCapacity":
	case "DesignVoltage":
	case "SpecificationInfo":
	case "ManufactureDate":
	case "SerialNumber":
		msg.BlockData = query("serial_number")
	case "ManufacturerName":
		msg.BlockData = query("manufacturer")
	case "DeviceName":
		msg.BlockData = query("model_name")
	case "DeviceChemistry":
		msg.BlockData = query("technology")
	case "ManufacturerData":
		// N/A
	default:
		panic("unknown message type in SBS")
	}
	return msg
}

